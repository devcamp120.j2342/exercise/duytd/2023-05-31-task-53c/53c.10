import models.Rectangle;
import models.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
        
        Rectangle rectangle1 = new Rectangle("red",10, 20);
        Rectangle rectangle2 = new Rectangle("green",15, 20);
        
        
        System.out.println(rectangle1.getArea());
        System.out.println(rectangle2.getArea());
        System.out.println(rectangle1.toString());
        System.out.println(rectangle2.toString());

        Triangle triangle1 = new Triangle("blue",5, 8);
        Triangle triangle2 = new Triangle("orange",10, 16);

        System.out.println(triangle1.getArea());
        System.out.println(triangle2.getArea());
        System.out.println(triangle1.toString());
        System.out.println(triangle2.toString());
    }
}
