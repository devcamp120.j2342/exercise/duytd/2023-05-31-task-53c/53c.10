package models;

public abstract class Shape {
    private String color;

    @Override
    public String toString() {
        return "Shape [color=" + color + "]";
    }

    public Shape(String color) {
        this.color = color;
    }

    public abstract double getArea();
}
